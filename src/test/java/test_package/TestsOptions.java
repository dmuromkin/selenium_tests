package test_package;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestsOptions {

    WebDriver driver;

    @BeforeMethod
    public void beforeTests() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options=new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1920,1080");
        //System.setProperty("webdriver.chrome.driver", "C://Program Files (x86)//chromedriver.exe");
        driver=new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("http://158.160.8.77:8888/");
    }

    @AfterMethod
    public void afterTests() {
        driver.quit();
    }

}