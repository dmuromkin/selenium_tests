package test_package;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

@Test
public class BaseTests extends TestsOptions {

    @Test
    public void features_on_map() throws InterruptedException {

        Thread.sleep(3000);
        driver.get("http://158.160.8.77:8600/geoserver/spbget//wfs?service=WFS&version=1.1.0&request=GetFeature&typename=spbget:trams_span,spbget:trams_stations&outputFormat=application/json&srsname=EPSG:3857&bbox=3227939.672385256,8338125.777576547,3493329.034591388,8393466.186055014,EPSG:3857");
        var response_data = driver.findElement(By.tagName("pre")).getAttribute("innerHTML");
        assertTrue(response_data.contains("totalFeatures"));
        var totalFeatures = response_data.split("\"totalFeatures\":")[1].split(",")[0];
        assertTrue(Integer.parseInt(totalFeatures)>0);

        Thread.sleep(3000);
        driver.get("http://158.160.8.77:8600/geoserver/spbget//wfs?service=WFS&version=1.1.0&request=GetFeature&typename=spbget:trolleys_span,spbget:trolleys_stations&outputFormat=application/json&srsname=EPSG:3857&bbox=3227939.672385256,8338125.777576547,3493329.034591388,8393466.186055014,EPSG:3857");
        response_data = driver.findElement(By.tagName("pre")).getAttribute("innerHTML");
        assertTrue(response_data.contains("totalFeatures"));
        totalFeatures = response_data.split("\"totalFeatures\":")[1].split(",")[0];
        assertTrue(Integer.parseInt(totalFeatures)>0);
    }

    @Test
    public void transport_moves() throws InterruptedException {

        Thread.sleep(3000);
        driver.findElement(By.xpath("//a[@href='/trafficonroute']")).click();
        Thread.sleep(10000);
        var route = driver.findElements(By.xpath("//div[@class='VictoryContainer']")).get(0);
        var vehicles = route.findElements(By.tagName("svg"));
        ArrayList<String> coords = new ArrayList<>();
        for (var veh: vehicles){
            coords.add((veh.getAttribute("x")));
        }
        System.out.println(coords);
        Thread.sleep(15000);
        route = driver.findElements(By.xpath("//div[@class='VictoryContainer']")).get(0);
        vehicles = route.findElements(By.tagName("svg"));
        ArrayList<String> coords_new = new ArrayList<>();
        for (var veh: vehicles){
            coords_new.add((veh.getAttribute("x")));
        }
        System.out.println(coords_new);
        assertNotEquals(coords,coords_new);
    }

}